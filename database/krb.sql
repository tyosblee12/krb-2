-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 10, 2020 at 04:50 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `krb`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_barang`
--

CREATE TABLE `tb_barang` (
  `id_brg` varchar(50) NOT NULL,
  `nama_brg` varchar(255) NOT NULL,
  `stock` int(25) NOT NULL,
  `harga` int(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_barang`
--

INSERT INTO `tb_barang` (`id_brg`, `nama_brg`, `stock`, `harga`) VALUES
('101', 'Fresh Tea', 499, 3000),
('102', 'Coca Cola', 8, 3500),
('103', 'Teh Pucuk', 98, 4000),
('104', 'Kopikap', 125, 2000);

-- --------------------------------------------------------

--
-- Table structure for table `tb_pelanggan`
--

CREATE TABLE `tb_pelanggan` (
  `id_plg` varchar(50) NOT NULL,
  `nama_plg` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_pelanggan`
--

INSERT INTO `tb_pelanggan` (`id_plg`, `nama_plg`) VALUES
('6401', 'Aldi Wijaya Yusuf'),
('6402', 'Randy Yohanes Sihombing'),
('6403', 'Sandy Akhmad Ramdiansyah'),
('6404', 'M Abdulah Algifari'),
('6405', 'Rayci Muhcidfudin'),
('6406', 'Septianus Cahyadi');

-- --------------------------------------------------------

--
-- Table structure for table `tb_penjualan`
--

CREATE TABLE `tb_penjualan` (
  `id_jual` varchar(50) NOT NULL,
  `nm_petugas` varchar(255) NOT NULL,
  `id_plg` varchar(50) NOT NULL,
  `id_brg` varchar(50) NOT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `ket` text NOT NULL,
  `tanggal` date NOT NULL,
  `total` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_penjualan`
--

INSERT INTO `tb_penjualan` (`id_jual`, `nm_petugas`, `id_plg`, `id_brg`, `jumlah`, `ket`, `tanggal`, `total`) VALUES
('500', 'Setyo Dwi Cahyo', '6401', '101', 1, 'Kasbon', '2020-12-10', 3000),
('501', 'Setyo Dwi Cahyo', '6402', '102', 2, 'Kasbon', '2020-12-10', 7000),
('502', 'Setyo Dwi Cahyo', '6403', '103', 2, 'Cash', '2020-12-10', 8000);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id_user` int(10) UNSIGNED NOT NULL,
  `nama` varchar(200) NOT NULL,
  `password` varchar(50) NOT NULL,
  `username` varchar(200) NOT NULL,
  `level_user` varchar(150) NOT NULL DEFAULT 'member'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id_user`, `nama`, `password`, `username`, `level_user`) VALUES
(901, 'Setyo Dwi Cahyo', 'admin', 'admin', 'admin'),
(902, 'Saila Saadiah Amanatillah', 'member', 'member', 'member');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_barang`
--
ALTER TABLE `tb_barang`
  ADD PRIMARY KEY (`id_brg`);

--
-- Indexes for table `tb_pelanggan`
--
ALTER TABLE `tb_pelanggan`
  ADD PRIMARY KEY (`id_plg`);

--
-- Indexes for table `tb_penjualan`
--
ALTER TABLE `tb_penjualan`
  ADD PRIMARY KEY (`id_jual`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=905;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
