<?php
session_start();
include "../config.php";

$username = $_SESSION['username'];
$nama_user = mysqli_query($koneksi, "SELECT nama FROM users WHERE username = '$username'");
$data = mysqli_fetch_array($nama_user);

$kode = mysqli_query($koneksi, "SELECT * FROM users");
$kodeotomatis = mysqli_num_rows($kode);
$akhir = $kodeotomatis + 1;

include "../header.php";
?>
<!-- ============================= AKHIR DARI BAR ATAS ============================= -->
<!-- Begin Page Content -->
<div class="container-fluid ">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Dashboard Data Petugas</h1>
        <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-warning shadow-sm"><i
                class="fas fa-user-plus  fa-sm text-black-50"></i> TAMBAH PETUGAS</a>
    </div>
    <!-- TAMBAH PETUGAS -->
    <div class="card shadow mb-4">
        <!-- Card Header - Accordion -->
        <a href="#collapseCardExample" class="d-block card-header py-3 bg-primary" data-toggle="collapse" role="button"
            aria-expanded="true" aria-controls="collapseCardExample">
            <h6 class="m-0 font-weight-bold text-white ">Tambah Data Petugas</h6>
        </a>
        <!-- Card Content - Collapse -->
        <div class="collapse hide" id="collapseCardExample">
            <div class="card-body">
                <form action="tpetugas.php" method="POST">
                    <div class="row">
                        <div class="col col-md-1">
                            <label for="id_petugas">ID User</label>
                            <input type="text" class="form-control" name="id_user" placeholder="ID"
                                value=" <?php echo $akhir; ?>" readonly>

                        </div>
                        <div class="col col-md-3">
                            <label for="id_petugas">Nama Petugas</label>
                            <input type="text" class="form-control" name="nama" placeholder="Nama Lengkap">
                        </div>
                        <div class="col col-md-3">
                            <label for="id_petugas">Username</label>
                            <input type="text" name="username" class="form-control" id="inlineFormInputGroup"
                                placeholder="Username">
                        </div>
                        <div class="col col-md-3">
                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" name="password" class="form-control" id="inlineFormInputGroup"
                                    placeholder="Password">
                            </div>
                        </div>
                        <div class="col col-md-2">
                            <label for="id_petugas">Level</label>
                            <select class="custom-select" name="level_user">
                                <option value='' selected>- Pilih -</option>
                                <option value='admin'>Admin</option>
                                <option value='member'>Member</option>
                                <option value='redaksi'>Redaksi</option>
                            </select>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary mt-3 float-right mb-3" name="submit">Simpan</button>
                </form>
            </div>
        </div>
    </div>

    <!-- Content Row -->
    <div class="row">
        <div class="col-xl-12 col-md-6 mb-4 animated--grow-in">
            <div class="card  shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <table class="table table-bordered table-hover">
                                <thead class="thead-dark">
                                    <tr class="text-dark">
                                        <!-- <th scope="col">No</th> -->
                                        <th scope="col" class="row-md-1">ID</th>
                                        <th scope="col" class="row-md-5">Nama</th>
                                        <th scope="col" class="row-md-3">Username</th>
                                        <th scope="col" class="row-md-1">Level</th>
                                        <th scope="col" class="row-md-2">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
include '../config.php';
// $no = 1;
$data = mysqli_query($koneksi, "select * from users");
while ($d = mysqli_fetch_array($data)) {
    ?>
                                    <tr>
                                        <!-- <td><?php echo $no++; ?></td> -->
                                        <td><?php echo $d['id_user']; ?></td>
                                        <td><?php echo $d['nama']; ?></td>
                                        <td><?php echo $d['username']; ?></td>
                                        <td><?php echo $d['level_user']; ?></td>
                                        <td>
                                            <a class=" d-none d-sm-inline-block btn btn-sm btn-success shadow-sm"
                                                href="upetugas.php?id_user=<?php echo $d['id_user']; ?>"> <i
                                                    class="fas fa-pen fa-sm text-black-50"></i> Ubah</a>
                                            <a class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm"
                                                href="hpetugas.php?id_user=<?php echo $d['id_user']; ?>"> <i
                                                    class="far fa-trash-alt fa-sm text-black-50"></i> Hapus</a>
                                        </td>
                                    </tr>
                                    <?php
}
?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Footer -->
        <?php
include '../footer.php';
?>
        <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

</body>

<!-- Bootstrap core JavaScript-->
<script src="../vendor/jquery/jquery.min.js"></script>
<script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="../style/js/sb-admin-2.min.js"></script>

<!-- Page level plugins -->
<script src="../vendor/chart.js/Chart.min.js"></script>

<!-- Page level custom scripts -->
<script src="../style/js/demo/chart-area-demo.js"></script>
<script src="../style/js/demo/chart-pie-demo.js"></script>

</html>