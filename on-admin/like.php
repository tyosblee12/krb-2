<?php
session_start();
include "../config.php";
$username = $_SESSION['username'];
$nama_user = mysqli_query($koneksi, "SELECT nama FROM users WHERE username = '$username'");
$data = mysqli_fetch_array($nama_user);

$databarang = mysqli_query($koneksi, "SELECT * FROM tb_barang");
$jumlahbarang = mysqli_num_rows($databarang);

$datapenjualan = mysqli_query($koneksi, "SELECT * FROM tb_penjualan");
$hasiljual = mysqli_num_rows($datapenjualan);

// SUBTOTAL
$barang = mysqli_fetch_array(mysqli_query($koneksi, "SELECT SUM(total) as Total_Bayar FROM `tb_penjualan` WHERE ket LIKE 'cash' "));
$data_barang = array('Total_Bayar' => $barang['Total_Bayar']);

// $kode = mysqli_query($koneksi, "SELECT * FROM tb_penjualan");
// $kodeotomatis = mysqli_num_rows($kode);
// $akhir = $kodeotomatis;

include_once "../header.php"
?>
<div class="container-fluid">
    <div class="row">
        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-3 col-md-6 mb-4 animated--grow-in">
            <div class="card border-left-danger shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">
                                Penghasilan (Annual)</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">
                                Rp. <?php echo $data_barang['Total_Bayar']; ?></div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-wallet fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-3 col-md-6 mb-4 animated--grow-in">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                Data Order</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $hasiljual; ?></div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-dolly fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-3 col-md-6 mb-4 animated--grow-in">
            <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Jumlah Barang
                            </div>
                            <div class="row no-gutters align-items-center">
                                <div class="col-auto">
                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">
                                        <?php echo $jumlahbarang; ?> Buah</div>
                                </div>
                                <div class="col">
                                    <div class="progress progress-sm mr-2">
                                        <div class="progress-bar bg-info progress-bar-animated progress-bar-striped"
                                            role="progressbar" style="width: <?php echo $jumlahbarang; ?>0%"
                                            aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-box-open fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Pending Requests Card Example -->
        <div class="col-xl-3 col-md-6 mb-4 animated--grow-in">
            <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                Pending Requests</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">18</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-comments fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class=" col-xl-12 col-lg-7">
        <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-dark">Input Data Baru</h6>
            </div>
            <form class="col-12">
                <table class="table table-bordered table-hover table-striped">
                    <thead class=" bg-info text-white">
                        <tr>
                            <th scope="col" class="row-md-0">No</th>
                            <th scope="col" class="row-md-0">ID Jual</th>
                            <th scope="col" class="row-md-5">Nama Petugas</th>
                            <th scope="col" class="row-md-0">ID-PLG</th>
                            <th scope="col">Nama Pelanggan</th>
                            <th scope="col">ID Barang</th>
                            <th scope="col">Nama Barang</th>
                            <th scope="col">Jumlah</th>
                            <th scope="col">Ket</th>
                            <th scope="col">Tanggal</th>
                            <th scope="col">Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
include '../config.php';

$barang = mysqli_fetch_array(mysqli_query($koneksi, "SELECT SUM(total) as Total_Bayar FROM `tb_penjualan` WHERE id_plg ='PLG-01' AND ket LIKE 'Cash' "));
$data1 = array('Total_Bayar' => $barang['Total_Bayar']);

$no = 1;
$data = mysqli_query($koneksi, "select * from tb_penjualan where id_plg = 'PLG-01' AND ket LIKE 'Cash' ");
while ($d = mysqli_fetch_array($data)) {
    ?>
                        <tr>
                            <td><?php echo $no++; ?></td>
                            <td><?php echo $d['id_jual']; ?></td>
                            <td><?php echo $d['nm_petugas']; ?></td>
                            <td><?php echo $d['id_plg']; ?></td>
                            <td><?php echo $d['nm_plg']; ?></td>
                            <td><?php echo $d['id_brg']; ?></td>
                            <td><?php echo $d['nm_brg']; ?></td>
                            <td><?php echo $d['jumlah']; ?></td>
                            <td><?php echo $d['ket']; ?></td>
                            <td><?php echo $d['tanggal']; ?></td>
                            <td>Rp. <?php echo $d['total']; ?></td>
                        </tr>
                        <?php
}
?>
                        <tr class="row-md-8">
                            <td class="font-weight-bold "> Subtotal : <?php echo $data1 ['Total_Bayar']; ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
        </div>
        </form>
    </div>
</div>