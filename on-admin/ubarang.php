<?php
session_start();
include "../config.php";

$username = $_SESSION['username'];
$nama_user = mysqli_query($koneksi, "SELECT nama FROM users WHERE username = '$username'");
$data = mysqli_fetch_array($nama_user);

include "../header.php";
$id_brg = $_GET['id_brg'];
$query_mysqli = $koneksi->query("SELECT * FROM tb_barang WHERE id_brg='$id_brg'") or die(mysqli_error());


// AMBIL DATA
$databrg = mysqli_fetch_array(mysqli_query($koneksi, "select * from tb_barang where id_brg='$_GET[id_brg]'"));
$data_brg = array('id_brg' => $databrg['id_brg'],
    'nama_brg' => $databrg['nama_brg'],
    'stock' => $databrg['stock'],
    'harga' => $databrg['harga']);

while ($data = mysqli_fetch_array($query_mysqli)) {
    ?>
<div class=" col-xl-6 col-lg-7">
    <div class="card shadow mb-4">
        <!-- Card Header - Dropdown -->
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-dark">Ubah Data Barang</h6>
        </div>

        <form class="col-md-12 p-3 mb-3 " method="POST">
            <div class="row m-3">
                <div class="col col-md-12">
                    <label for="id_brg">ID Barang</label>
                    <input type="text" class="form-control" name="id_brg" value="<?php echo $databrg['id_brg']; ?>"
                        readonly>
                </div>
                <div class="col col-md-12">
                    <label for="nama_brg">Nama Barang</label>
                    <input type="text" class="form-control" name="nama_brg" value="<?php echo $databrg['nama_brg']; ?>">
                </div>
                <div class="col col-md-12">
                    <label for="stock">Stock</label>
                    <input type="text" class="form-control" name="stock" value="<?php echo $databrg['stock']; ?>">
                </div>
                <div class="col col-md-12">
                    <label for="harga">Harga</label>
                    <input type="text" class="form-control text-right" name="harga"
                        value="<?php echo $databrg['harga']; ?>">

                    <!-- <button type="button" class="btn btn-secondary mt-3 float-right ml-3" name="kembali">
                        Kembali</button> -->
                    <button type="submit" class="btn btn-warning mt-3 float-right" name="submit"> Ubah</button>
                </div>
            </div>
        </form>
        <?php }?>
    </div>
</div>

<?php
include '../config.php';
if (isset($_POST['submit'])) {
    try {
        $id_brg = $_POST['id_brg'];
        $nama_brg = $_POST['nama_brg'];
        $stock = $_POST['stock'];
        $harga = $_POST['harga'];

        mysqli_query($koneksi, "UPDATE tb_barang SET id_brg='$id_brg', nama_brg='$nama_brg', stock='$stock', harga='$harga' WHERE id_brg='$id_brg'");
        print "<script>alert('Berhasil Mengubah Data $id_brg')
        window.location = 'dbarang.php';
        </script>";

    } catch (Exception $e) {
        echo $e->getMessage();
    }
}
?>


<!-- Footer -->
<?php
include '../footer.php';

?>
<!-- End of Footer -->
<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>
</body>
<!-- Bootstrap core JavaScript-->
<script src="../vendor/jquery/jquery.min.js"></script>
<script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../style/js/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="../style/js/sb-admin-2.min.js"></script>

<!-- Page level plugins -->
<!-- <script src="../vendor/chart.js/Chart.min.js"></script> -->

<!-- Page level custom scripts -->
<!-- <script src="../style/js/demo/chart-area-demo.js"></script>
<script src="../style/js/demo/chart-pie-demo.js"></script> -->

</html>