<?php
include_once "../header.php";
?>
<div class="container-fluid">
    <div class="row animated--grow-in">

        <!-- LIST KIRI -->
        <div class="col-xl-6 col-lg-7">
            <div class="card shadow mb-4 ">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-dark">KIRI</h6>
                </div>
                ISI DISINI
            </div>
        </div>
        <!-- END OF LIST KIRI -->

        <!-- LIST KANAN -->
        <div class="col-xl-6 col-lg-7">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-dark">KANAN</h6>
                </div>
                ISI DISINI

            </div>
        </div>
        <!-- LIST KANAN -->

    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="../vendor/jquery/jquery.min.js"></script>
    <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="../style/js/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="../style/js/sb-admin-2.min.js"></script>